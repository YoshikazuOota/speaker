## 使い方
### 初期設定
- windows に nodeをインストール
- コマンドプロンプトを開く
- npm install を実行(setting.bat)

### 使い方
- node server.js を実行(start.bat)
- 指定voice channelにBotが入るのを確認
- デフォルトマイクデバイスの音声がvoice channelのBotで再生される

## License
MIT License または Public Domain
* Public ドメインの場合でも、Qiita記事や著作者表記をしていただけると幸いです
