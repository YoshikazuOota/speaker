const recorder = require('node-record-lpcm16');

// ログイン処理
const config = require("./config/discord");
const Discord = require('discord.js');
const client = new Discord.Client();
const token = config.token;

client.on('ready', () => {
    const voiceChannel = client.channels.find(val => val.id === config.channel_id); // 指定テキストチャンネルを取得
    if (!voiceChannel || voiceChannel.type !== 'voice') {
        console.log("select channel is not voice channel");
    }

    // voice channelに接続
    voiceChannel.join()
        .then(conn => {
            console.log('connect voice channel');
            const streamOptions = { seek: 0, volume: 1 };
            inputStream = recorder.record({
                sampleRate: 48000
            }).stream();
            const dispatcher = conn.playStream(inputStream, streamOptions);
        })
        .catch(console.log);
});
client.login(token);

// ctrl-c 終了の際、全てのチャンネルから離脱(これがないとボットが timeoutするまで残ってしまう)
process.on('SIGINT', function () {
    console.log("Caught interrupt signal");
    client.destroy();
    process.exit();
});